#!/usr/bin/env node

'use strict';

var connect = require('connect');
var serveStatic = require('serve-static');
var open = require("open");
var path = require("path");

var app = connect()
var server = require('http').createServer(app)
var io = require('socket.io')(server)

// io gets mounted on socket.io first to prevent conflict with static files
io.on('connection', function(socket) {
	console.log('Connected to client')
})

// Collection from SensorLog
var bodyParser= require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
app.use('/sensor-log', function (req, res) {
	if(req.method === 'post') {
 	  var doc = req.body
	  if(io.sockets) {
		io.sockets.emit('newsensorlogdata', doc)
		//just monitoring that the iphone sends data
	    process.stdout.write(Math.floor(doc.locationMagneticHeading)+'.')
	  }
	}
})

// Static files + let's go
app.use('/scripts', __dirname + "/../node_modules");

var contentDir = path.join(__dirname, "../dist");

app.use(serveStatic(contentDir))
   .listen(8081, function() {
    console.log('Serving ' + contentDir);
    console.log('Server running on 8081 ...');
    open("http://localhost:8081");
});