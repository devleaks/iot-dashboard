#!/usr/bin/env node

// Setup basic express server
var express = require('express')
var app = express()
var server = require('http').createServer(app)
var io = require('socket.io')(server)
const port = 8081

// io gets mounted on socket.io first to prevent conflict with static files
io.on('connection', function(socket) {
	console.log('Connected to client')
})

const path = require('path')
const exec = require('child_process').exec;
exec('git describe --tags', (error, stdout, stderr) => {
  if (error) {
    console.error(`exec error: ${error}`)
    return
  }
  var me = path.basename(process.argv[1])
  console.log(`${me}: ${stdout}`)
});


// Routing
app.use('/', express.static(__dirname + '/../dist'))
app.use('/scripts', express.static(__dirname + '/../node_modules'))
app.use('/assets', express.static(path.join(__dirname, '/../node_modules/@salesforce-ux/design-system/assets')));

// Collection from SensorLog
var bodyParser= require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
app.post('/sensor-log', function (req, res) {
	 	var doc = req.body
	  if(io.sockets) {
			io.sockets.emit('newsensorlogdata', doc)
			//just monitoring that the iphone sends data
		    process.stdout.write(Math.floor(doc.locationMagneticHeading)+'.')
		}
})

// Let's go
server.listen(port, function () {
  console.log('Server listening at port %d', port)
})