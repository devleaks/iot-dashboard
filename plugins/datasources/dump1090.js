// We wrap the code into an anonymous function that is executed immediately to not pollute the global namespace.
// More about JavaScript Module pattern: http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html
(function () {

    // First define a JavaScript object with metadata for the plugin.
    // The object must be serializable, i.e. there must not be any function definitions inside
    const TYPE_INFO = {
        // The type is the primary key of the plugin an must be globally unique
        type: 'dump1090',
        // The name is displayed to the user to name the Plugin
        name: 'Dump 1090',
        // The kind is 'datasource' or 'widget' and used internally
        kind: 'datasource',
        // Take some credits in the author field
        author: 'Pierre M',
        // Use semantic version to version your plugins, it helps to keep track of changes
        version: '0.0.1',
        // A Description is shown in the UI to explain what the plugin does
        description: 'Receive aircarft positions from DUMP1090 software',
        // JavaScript and CSS dependencies can be loaded here
        // We do not load jQuery here because it's provided by the Dashboard - This will change in future!
        dependencies: [
			'/scripts/jquery/dist/jquery.min.js'
				],
        // The settings array describes the options shown in the config dialog of the plugin
        settings: [
            {
                // The id is unique for each plugin and is used to reference the setting value in code
                id: 'dump1090flavor',
                // The name is displayed next to the field
                name: 'Dump 1090 Flavor',
                // There are several types, here we let the user choose one of multiple options from a drop down box
                type: 'option',
                // The default value is referencing the 'value' that is selected by default
                defaultValue: 'malcolmrobb',
                options: [
                    // Each option can have a name and a value, where the name is displayed to the user and the value is used in code
                    {name: 'Original', value: 'antirez'},
                    {name: 'Malcolm Robb', value: 'malcolmrobb'},
                    {name: 'Mutability', value: 'mutability'},
                    {name: 'Ted Sluis', value: 'tedsluis'}
                ]
            },
            {
                id: 'url',
                name: "URL",
                // The string type just renders a string input field
                type: 'string',
                // The description is optional and shows a little hint icon with details about the setting
                description: 'URL for data feed',
                defaultValue: 'http://imac.local:8082/data.json' // 'http://localhost:8080/data.json'
            }
        ]
    };

    // Plugins are compiled with Babel
    // This means we can use ES6 JavaScript features like classes and do not have to care about older browsers. Babel will make it work!
    class Plugin {

        initialize(props) {
	            // Any code that is required to execute when the plugin is loaded the first time e.g. setting up some properties
            // Set how often fetchData() will be called, 'Infinite' to disable regular updates
            props.setFetchInterval(10 * 1000); // Fetch every 10 seconds, and once after the plugin is loaded.
            props.setFetchReplaceData(true);
        }

        // The only required function
        fetchData(resolve, reject) {
            // Return data via the resolve callback, that should be rendered by Widgets
            // Data must be an 'array' containing 'objects', e.g. [{temp: 23.5}, {temp: 23.3}]
            // resolve and reject are callbacks, to allow returning data from async callbacks
            // Per default, resolved data is appended to the datasource (see props.setFetchReplaceData above)
            // fetchData() is called when the datasource is created, after settings changed and regularly depending on the value of setFetchInterval(intervalInMs: number)

            // We can always access the datasource properties we saw in the constructor via this.props
            // Via the state we can access the setting values set by the user

						/*
            const settingValues = this.props.state.settings;

            const request = new Request('http://imac.local:8082/data.json', { /*mode: "no-cors"* /})

            fetch(request)
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
										console.log('dump1090', data)
                    resolve(data)
                })
						reject(new Error('no se?'))
						*/

						// console.log('ajax..')
						$.ajax({
							url: 'http://imac.local:8082/data.json',
							success: function(data) {
								// console.log('success ajax', data)
								var planes = [];
								data.forEach( function(plane) {
									if(plane['validposition'] !== "undefined" && plane['validposition'] == 1) {
										planes.push({
										  "type": "Feature",
										  "geometry": {
										    "type": "Point",
										    "coordinates": [parseFloat(plane['lon']), parseFloat(plane['lat']), parseFloat(plane['altitude'])]
										  },
										  "properties": plane
										})
									}
								})
								if(planes.length > 0) {
									//console.log('planes',planes)
									resolve(planes)
								} else {
									console.log('no valid plane')
									reject()
								}
							},
							error: function(jqXHR, textStatus, errorThrown) {
								reject(new Error('dump1090: Ajax error: '+textStatus+'/'+errorThrown));
							}
						})
						// console.log('..done.')
        }
    }

    // Now we just have to handover our TYPE_INFO object and Plugin class to the dashboard API to load them
    window.iotDashboardApi.registerDatasourcePlugin(TYPE_INFO, Plugin)
})();
