/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// SensorLog = require ('../libs/sensorLog')

(function (window) {
    const TYPE_INFO = {
        type: "sensor-log",
        name: "SensorLog Data",
        version: "0.0.3",
        description: "Datasource that provides from iOS Sensor Log App",
        dependencies: [
	    	 '/scripts/socket.io-client/socket.io.js'
			,'/plugins/libs/sensorLog.js'
        ]
    };

    class Datasource {
	
		constructor() {
			this.sensorData = []
		}
		
		getSensorData() {
			return this.sensorData
		}
	
        initialize(props) {
            props.setFetchInterval(10000)
            props.setFetchReplaceData(false)
			var sensorData = this.getSensorData();
			var socket = io.connect()
			
			socket.on('connect', function() {
				console.log("sensor-log: Connected to server")
			});
		
			socket.on('newsensorlogdata', function(doc) {
				var feat = SensorLog.makeGeoJsonSensorData(doc)				
				if(feat) {
					sensorData.push(feat)
					// console.log(feat)
				}
			});

        }

        fetchData(resolve, reject) {
			var sensorData = this.getSensorData();
			if (sensorData.length > 0) { // sends all available data and resets array
				resolve(sensorData.splice(0,sensorData.length))
			} else { // no data
			 	reject()
			}
        }

        dispose() {
			var sensorData = this.getSensorData();
            sensorData = [];
            console.log("SensorLog Datasource destroyed");
        }

    }

    window.iotDashboardApi.registerDatasourcePlugin(TYPE_INFO, Datasource);

})(window);
