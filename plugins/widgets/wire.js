/**
	Timeline Widget for IOT-Dashboard
	Feed with Timeline JSON object
	{
		title:
		link:
		subject:
		body:
		priority:
		tags:
		source:
		type:
		timestamp:

		infos: { // info line {"name: value"}
			name:
			text:
			link:
			help:
		}

		actions: { // for pull-down menu
			name:
			link:
			help:
		}

		sldsType: // {call,email,event,task}, sets the "color" of the event and its info, fa-icons: comment-o,envelope-o,calendar,tasks.
	}
	
 */

// import { Icon } from 'react-lightning-design-system';

(function () {

    const TYPE_INFO = {
        type: "timeline",
        name: "Timeline",
        version: "0.0.1",
        rendering: "react",
        description: "Render Timeline and adds events as they arrive.",
        dependencies: [
        	 '/assets/styles/salesforce-lightning-design-system.min.css'
			,'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'
//			, '/scripts/react-lightning-design-system/lib/scripts/Icon.js'
        ],
        settings: [
            {
                id: 'datasource',
                name: 'Datasource',
                type: 'datasource',
                description: "Datasource to get the events from"
            },
        ]
    }


	class TimelineItemCheckbox extends React.Component {		
		render() {
			return <div className="slds-media__figure">
              <label className="slds-checkbox" for="mark-complete">
                <input name="checkbox" type="checkbox" id="mark-complete" />
                <span className="slds-checkbox--faux"></span>
                <span className="slds-form-element__label slds-assistive-text">Mark Complete</span>
              </label>
            </div>
		}
	}

	class TimelineItemPNGIcon extends React.Component {
		render() { // /assets/icons/utility-sprite/svg/symbols.svg#down
			return <div className="slds-media__figure slds-timeline__icon">
	        	<div className="slds-icon_container slds-icon-text-warning" style={ {width: "24px", height: "24px", backgroundColor: "lightblue"} }>
					<image src={"/assets/icons/" + this.props.category + '/' + this.props.icon + "_120.png"} style={ {width: "100%", height: "100%"} } />
				</div>
	        </div>
		}
	}
	
	class TimelineItemIcon extends React.Component {
		render() {
			return <div className="slds-media__figure slds-timeline__icon">
	        	<div className="slds-icon_container">
					<svg aria-hidden="true" className="slds-icon">
			    		
					</svg>
				</div>
	        </div>
		}
	}
	
	class TimelineFAIcon extends React.Component {
		render() {
			return <div className="slds-media__figure slds-timeline__icon">
	          <div className="slds-icon_container slds-icon slds-icon--small">
	            <i className={"fa fa-" + this.props.icon}></i>
	          </div>
	        </div>
		}
	}
	
	class TimelineItemDateAndActions extends React.Component{
		render() {
			return <div className="slds-media__figure slds-media__figure--reverse">
		      <div className="slds-timeline__actions">
		        <p className="slds-timeline__date">{this.props.date}</p>
			    <TimelineItemActions actions={this.props.actions} />
		      </div>
		    </div>
		}
	}

	class TimelineItemAction extends React.Component{		
		render() {
			return <li className="slds-dropdown__item" role="presentation">
		        <a href={this.props.action.link} role="menuitem" tabindex="-1">
		        	<span className="slds-truncate">{this.props.action.name}</span>
		        </a>
		     </li>
		}
	}

	class TimelineItemActions extends React.Component{		
		render() {
			if (! this.props.actions) {
				return null;
			} else {
				return <div className="slds-dropdown-trigger slds-dropdown-trigger--click">
					<button className="slds-button slds-button--icon-border-filled slds-button--icon-x-small" aria-haspopup="true">
			        	<span><TimelineFAIcon icon="caret-down" /></span>
			        	<span className="slds-assistive-text">More Options</span>
			        </button>
			        <div className="slds-dropdown slds-dropdown--right">
					    <ul className="slds-dropdown__list" role="menu">
							{
								this.props.actions.map( function(action) {
									return <TimelineItemAction action={action} />
								})
							}
				    	</ul>
					</div>
				</div>
			}
		}
	}

	class TimelineItemInfo extends React.Component {		
		render() {
			return <li className="slds-m-right--large">
              <span className="slds-text-title">{this.props.info.name}</span>
              <span className="slds-text-body--small">{this.props.info.text}</span>
            </li>
		}
	}

	class TimelineItemInfos extends React.Component{		
        render() {
			console.log('TimelineItemInfos', this.props.infos)
            return (
				<ul>
					{
						this.props.infos.map( function(info) {
							return <TimelineItemInfo info={info} />
						})
					}
				</ul>
			)
        }
	}

	class TimelineItem extends React.Component {		
		render() {
			return <div className="slds-media">
			    <div className="slds-media__body">
			      <div className="slds-media slds-media--timeline slds-timeline__media--task">
					<TimelineItemPNGIcon category="standard" icon="task" />
			        <div className="slds-media__body">
			          <div className="slds-media">
			            <TimelineItemCheckbox />
			            <div className="slds-media__body">
			              <h3 className="slds-truncate" title={this.props.item.title}><a href={this.props.item.link}>{this.props.item.subject}</a></h3>
						  <TimelineItemInfos infos={this.props.item.infos} />
			            </div>
			          </div>
			        </div>
			      </div>
			    </div>
			    <TimelineItemDateAndActions date={this.props.item.date} actions={this.props.item.actions} />
			  </div>
		}
	}

    class Timeline extends React.Component {	
        render() {
			console.log('Timeline', this.props.items.length)
            return (
				<ul>
					{
						this.props.items.map( function(item) {
							return <TimelineItem item={item} />
						})
					}
				</ul>
			)
        }
    }

    class TimelineWidget extends React.Component {
        render() {
            const props = this.props
            const data = props.getData(props.state.settings.datasource)

            if (!data || data.length == 0) {
                return <p>No data</p>
            }

			console.log('TimelineWidget', data.length)

            return <div style={{width: '100%', height: '100%', padding: '1%'}}>
					<Timeline items={data} />
            </div>
        }
    }

	// TODO: Move to core, for simple reuse
    const Prop = React.PropTypes
    TimelineWidget.propTypes = {
        getData: Prop.func.isRequired,
        state: Prop.shape({
            height: Prop.number.isRequired,
            id: Prop.string.isRequired
        }).isRequired
    }

    window.iotDashboardApi.registerWidgetPlugin(TYPE_INFO, TimelineWidget)

})()