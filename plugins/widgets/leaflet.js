/**
	Leaflet Widget for IOT-Dashboard
	
	Same widget as Google Map widget, but uses leafletjs instead.
	
	Test with static datasource. Supply following data:
	
	{
	  "type": "FeatureCollection",
	  "features": [
	    {
	      "type": "Feature",
	      "properties": {},
	      "geometry": {
	        "type": "Polygon",
	        "coordinates": [
	          [
	            [
	              4.346294403076172,
	              50.85873522444264
	            ],
	            [
	              4.368267059326172,
	              50.852450002507354
	            ],
	            [
	              4.369468688964844,
	              50.84757295365389
	            ],
	            [
	              4.365863800048828,
	              50.84041902626522
	            ],
	            [
	              4.350585937499999,
	              50.83391450400326
	            ],
	            [
	              4.348354339599609,
	              50.83315558401709
	            ],
	            [
	              4.3450927734375,
	              50.833047165868834
	            ],
	            [
	              4.342517852783203,
	              50.8340229201368
	            ],
	            [
	              4.337711334228516,
	              50.84887354987344
	            ],
	            [
	              4.346294403076172,
	              50.85873522444264
	            ]
	          ]
	        ]
	      }
	    }
	  ]
	}


 */
(function () {

    const TYPE_INFO = {
        type: "leaflet-map",
        name: "Leaflet Map",
        version: "0.0.2",
        rendering: "react",
        description: "Render Leaflet Map with some Location Data",
        dependencies: [
            'https://unpkg.com/leaflet@1.0.1/dist/leaflet.js',
			'https://unpkg.com/leaflet@1.0.1/dist/leaflet.css',
			'https://unpkg.com/geojsonhint@latest/geojsonhint.js'
        ],
        settings: [
            {
                id: 'datasource',
                name: 'Datasource',
                type: 'datasource',
                description: "Datasource to get the gps values from"
            },
            {
                id: 'initLat',
                name: 'Initial Latitude',
                type: 'number',
                defaultValue: 50.604, // 50.8449882,
                description: "Latitude when the map is loaded"
            },
            {
                id: 'initLng',
                name: 'Initial Longitude',
                type: 'number',
                defaultValue: 4.211, // 4.3488835,
                description: "Longitude when the map is loaded"
            },
            {
                id: 'initZoom',
                name: 'Initial Zoom',
                type: 'number',
                defaultValue: 13,
                description: "Zoom level when the map is loaded"
            }
        ]
    };

    // The API is similar to React but it is actually NOT a react component.
    // On render you get the DOM "element" to renter the content.
    const LeafletWidget = React.createClass({
        getInitialState: function () {
            return {};
        },
        componentDidMount: function () {
            const settings = this.props.state.settings;
            const map = L.map(document.getElementById('map-' + this.props.state.id), {
                center: [settings.initLat, settings.initLng],
                zoom: Number(settings.initZoom)
            });
			L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);
            this.setState({map: map})
        },
        componentWillReceiveProps(nextProps) {
            if (nextProps.state.availableHeightPx != this.props.state.availableHeightPx) {
	            if (this.state.map) {
                	this.state.map.invalidateSize();
				}
            }
        },
        render: function () {
            const props = this.props;
            const settings = props.state.settings;

            const data = props.getData(settings.datasource);
			console.log('data length', data.length);

			const map = this.state.map
			
            if (map) {
				data.forEach( function(feature) {
					var geojsonErrors = geojsonhint.hint(feature, { precisionWarning: false });
					
					if(! geojsonErrors || geojsonErrors.length == 0) {
			            var geojsonLayer = L.geoJson(feature, {
						    pointToLayer: function (feature, latlng) {
						        return L.circleMarker(latlng, {
								    radius: 2,
								    fillColor: "#005",
									color: "#008",
								    opacity: 1,
								    fillOpacity: 0.5
								});
						    }
						}).addTo(map);
					} else {
						console.log('Invalid GeoJSON', feature, geojsonErrors);
					}
				});
            }			
            return React.DOM.div({id: 'map-' + props.state.id, style: {height: "100%"}});
        }
    });

    window.iotDashboardApi.registerWidgetPlugin(TYPE_INFO, LeafletWidget);

})();