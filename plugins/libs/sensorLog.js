"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (SensorLog) {

	SensorLog.version = '0.0.1';

	// Allow user to specify default parameters
	SensorLog.defaults = {
		// convertion of data to proper basic types
		toFloat: ["locationLatitude", "locationLongitude", "locationAltitude", "locationSpeed", "locationCourse", "locationHeadingX", "locationHeadingY", "locationHeadingZ", "locationTrueHeading", "locationMagneticHeading", "accelerometerAccelerationX", "accelerometerAccelerationY", "accelerometerAccelerationZ", "gyroTimestamp_sinceReboot", "gyroRotationX", "gyroRotationY", "gyroRotationZ", "motionYaw", "motionRoll", "motionPitch", "motionRotationRateX", "motionRotationRateY", "motionRotationRateZ", "motionUserAccelerationX", "motionUserAccelerationY", "motionUserAccelerationZ", "motionQuaternionX", "motionQuaternionY", "motionQuaternionZ", "motionQuaternionW", "motionGravityX", "motionGravityY", "motionGravityZ"],
		toInt: ["locationVerticalAccuracy", "locationHorizontalAccuracy", "locationHeadingAccuracy", "motionMagneticFieldX", "motionMagneticFieldY", "motionMagneticFieldZ", "motionMagneticFieldCalibrationAccuracy", "activityActivityConfidence", "pedometerNumberofSteps", "pedometerDistance", "pedometerFloorAscended", "pedometerFloorDescended", "deviceOrientation", "batteryState", "batteryLevel", "state"],
		roundToInt: ["locationTimestamp_since1970", "locationHeadingTimestamp_since1970", "motionTimestamp_sinceReboot", "accelerometerTimestamp_sinceReboot", "activityTimestamp_sinceReboot"]
	};

	function applyDefaults(params, defaults) {
		var settings = params || {};

		for (var setting in defaults) {
			if (defaults.hasOwnProperty(setting) && !settings[setting]) {
				settings[setting] = defaults[setting];
			}
		}

		return settings;
	}

	/**
  *	Alternative: Use https://github.com/caseypt/geojson.js.
  */
	SensorLog.makeGeoJsonSensorData = function (doc) {
		if (SensorLog.isSensorData(doc)) {
			//convert text to float
			SensorLog.defaults.toFloat.forEach(function (param) {
				doc[param] = parseFloat(doc[param]);
			});
			//convert text to int
			SensorLog.defaults.toInt.forEach(function (param) {
				doc[param] = parseInt(doc[param]);
			});
			//special
			SensorLog.defaults.roundToInt.forEach(function (param) {
				doc[param] = Math.floor(parseFloat(doc[param]));
			});

			return {
				"type": "Feature",
				"geometry": {
					"type": "Point",
					"coordinates": [doc['locationLongitude'], doc['locationLatitude'], doc['locationAltitude']]
				},
				"properties": doc
			};
		}
		return null;
	};

	SensorLog.getFeatureId = function (doc) {
		// this allow to simulate several devices with state added
		return doc.properties.deviceID + doc.properties.state;
	};

	SensorLog.isSensorData = function (doc) {
		// return doc instanceof SensorData
		return typeof doc['locationLatitude'] != "undefined";
	};

	SensorLog.isLoggedSensorData = function (doc) {
		// return o instanceof SensorData
		return typeof doc['loggingSample'] != "undefined";
	};
})((typeof module === "undefined" ? "undefined" : _typeof(module)) == 'object' ? module.exports : window.SensorLog = {});